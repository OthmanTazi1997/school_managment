from utils import *

def consultClasses():
    classes = open("MyClasses.txt")
    fileContent = classes.read()
    classes.close()
    return fileContent
    

def addClasses():

    classes = open("MyClasses.txt")
    classesListe = classes.readlines()
    classes.close()
    classesListe1 = "".join(classesListe)   
    print(classesListe1)

    newClasseN1 = input(" please enter a new classe : ")
    newClasse = "\n" + newClasseN1
    newClasseN0 =  newClasseN1 + "\n"
    newClasseN2 = "\n" + newClasseN1 + "\n"
    

    if (newClasse or newClasseN0 or newClasseN1 or newClasseN2) in (classesListe1 or classesListe) :
        print("\n this classe is already in the classes liste.")
        againAsk = input("\n do you still whant to add ? (y/n)")
        while againAsk not in ('y','n'):
            print("\n please enter a valid option (y/n)")
            againAsk = input("\n do you still whant to add ?")
        if againAsk == 'n':                          
            return "finished adding new classes"
        if againAsk == 'y':
            addClasses()
    else:
        classesListe.append(newClasse)
        addAsk = input("\n do you want to add an other classe ? (y/n)")
        while addAsk not in ('y','n'):
            print("\n please enter a valid option (y/n)")
            addAsk = input("\n do you still whant to add ? (y/n)")
        if addAsk == 'n':                         
            classes = open("MyClasses.txt","w")
            newClassesListe = "".join(classesListe)
            classes.write(newClassesListe)
            classes.close()
            return "finished adding new classes"
        if addAsk == 'y':
            classes = open("MyClasses.txt","w")
            newClassesListe = "".join(classesListe)
            classes.write(newClassesListe)
            classes.close()
            addClasses()

    
def modifyClasses() :
    classes = open("MyClasses.txt")
    classesListe = classes.readlines()
    classes.close()
    classesListe1 = "".join(classesListe) 
    print(classesListe1)

    classeToModifyN1 = input("input the classe you whant to modify : ")
    classeToModify = "\n" + classeToModifyN1 
    classeToModifyN0 =  classeToModifyN1 + "\n"
    if (classeToModify and classeToModifyN0 and classeToModify) not in classesListe1:
        print("\n this classe is not in the classe lists")
        addAsk = input("do you whant to add it to the list (this will redirect you to the adding classes interface) ? (y/n)")
        while addAsk not in ('y','n'):
            print("\n please enter a valid option (y/n)")
            addAsk = input("\n do you still whant to add ? (y/n)")
        if addAsk == 'n':                          
            continueModificationAsk = input("do you want to continue the modifications ? (y/n)")
            while continueModificationAsk not in ('y','n'):
                print("\n please enter a valid option (y/n)")
                continueModificationAsk = input("do you want to continue the modifications ? (y/n)")
            if continueModificationAsk == 'y':
                modifyClasses()
            if continueModificationAsk == 'n':
                return "end of modifications"

        if addAsk == 'y':
            addClasses()
    else:
        
        newModifiedClasse =  input("input your modification : ") + "\n"
        newModifiedClasseN1 = "\n" + newModifiedClasse
        if newModifiedClasseN1 not in classesListe1 :

            classeToModifyN2 = classeToModifyN1 + "\n"
            classesListe = [newModifiedClasse if modifyer == classeToModifyN2 else modifyer for modifyer in classesListe]
            classesListe1 = "".join(classesListe)
           
            classes = open("MyClasses.txt","w")
            classes.write(classesListe1)
            classes.close()

            againAsk = input("do you want to do an other modification ? (y/n)")
            while againAsk not in ('y','n'):
                print("\n please enter a valid option (y/n)")
                againAsk = input("\n do you want to do an other modification ? (y/n)")
            if againAsk == 'n' :
                return "finished modifying"
            if againAsk == 'y' :
                modifyClasses()

        else :
            print("this classe is already in the list of classes")
            againAsk = input("do you want to do an other modification ? (y/n)")
            while againAsk not in ('y','n'):
                print("\n please enter a valid option (y/n)")
                againAsk = input("\n do you want to do an other modification ? (y/n)")
            if againAsk == 'n' :
                return "finished modifying"
            if againAsk == 'y' :
                modifyClasses()

def removeClasse() :
    classes = open("MyClasses.txt")
    classesListe = classes.readlines()
    classes.close()
    classesListe1 = "".join(classesListe)
    print(classesListe1)
    classeToRemove = input("please input the classe to remove : ")
    classeToRemoveN1 = "\n" + classeToRemove
    classeToRemoveN =  classeToRemove + "\n"
    if (classeToRemoveN1 and classeToRemoveN) not in classesListe1 :
        print("this classe is not on the list")
        againAsk = input("do you whant to remove an other classe ? (y/n)")
        while againAsk not in ('y','n'):
            print("\n please enter a valid option (y/n)")
            againAsk = input("\n do you want to do remove an other classe ? (y/n)")
        if againAsk == 'n':
            return "finished deleting"
        if againAsk == 'y':
            removeClasse()
    else:
        classeToRemoveN2 = classeToRemove + "\n"
        classesListe.remove(classeToRemoveN2)

        classesListe1 = "".join(classesListe)            
        classes = open("MyClasses.txt","w")
        classes.write(classesListe1)
        classes.close()

        againAsk = input("do you whant to remove an other classe ? (y/n)")
        while againAsk not in ('y','n'):
            print("\n please enter a valid option (y/n)")
            againAsk = input("\n do you want to do remove an other classe ? (y/n)")
        if againAsk == 'n':
            return "finished deleting"
        if againAsk == 'y':
            removeClasse()

